
FROM alpine:3.9
LABEL maintainer="Bruno Paz <oss@brunopaz.net>"

# packages
RUN apk update && apk add --no-cache \
    git \
    openssh \
    bash \
    tzdata \
    gettext \
    curl \
    php \
    php-curl \
    php-dom \
    php-gettext \
    php-json \
    php-mbstring \
    php-openssl \
    php-pdo \
    php-phar \
    php-opcache \
    php-session \
    php-xml \
    php-zlib \
    php-ctype \
    php-iconv \
    && rm -rf /var/cache/apk/*

ENV COMPOSER_ALLOW_SUPERUSER=1

# php composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

RUN wget https://get.symfony.com/cli/installer -O - | bash

RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony

WORKDIR /home

ENTRYPOINT ["symfony"]
CMD ["--help"]