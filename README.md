# Docker Symfony Cli

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/brpaz/symfony-cli-docker.svg?style=for-the-badge)
![Docker Pulls](https://img.shields.io/docker/pulls/brpaz/symfony-cli.svg?style=for-the-badge)
![MicroBadger Layers](https://img.shields.io/microbadger/layers/brpaz/symfony-cli.svg?style=for-the-badge)
![MicroBadger Size](https://img.shields.io/microbadger/image-size/brpaz/symfony-cli.svg?style=for-the-badge)

> [Symfony Cli](https://symfony.com/download) Docker image.


## Usage


```sh
docker pull brpaz/symfony-cli
docker run --rm -it brpaz/symfony-cli new my-project --no-git
```

## Contributing

Contributions, issues and Features requests are welcome.

## Show your support

<a href="https://www.buymeacoffee.com/Z1Bu6asGV" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A Coffee" style="height: 41px !important;width: 174px !important;box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;" ></a>

## License 

Copywright @ 2019 [Bruno Paz](https://github.com/brpaz)

This project is [MIT](LLICENSE) Licensed.
